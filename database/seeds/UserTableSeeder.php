<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

        DB::table('users')->delete();
        
        User::create( [
            'name'=>'Test Account',
            'email'=>'abc@xyz.com',
            'password' => bcrypt('12345') ]);
	}

}

// c:> composer dump-autoload
// c:> php artisan db:seed
@extends('base')

<style>
	body {
		margin: 0;
		padding: 0;
		width: 100%;
		height: 100%;
		color: #B0BEC5;
		display: table;
		font-weight: 100;
		font-family: 'Lato';
	}

	.container {
		text-align: center;
		display: table-cell;
		vertical-align: middle;
	}

	.content {
		text-align: center;
		display: inline-block;
	}

	.title {
		font-size: 96px;
		margin-bottom: 40px;
	}

	.quote {
		font-size: 24px;
	}
</style>
	
@section('content')
    <div class="row content">
    	<div class="title highlight col-xs-12 col-sm-12 col-md-12">Laravel 5 Blade</div>
    </div>
    <div class="row"><div class="col-md-12">{{ $data }}</div></div>
    <div class="row">
        <div class="col-md-12">
    		@forelse( $products as $product )
    		    {{ $product }}<br />
    		@empty
    		    <!-- -->
    		@endforelse
        </div>
    </div>
@stop





@extends('base')

@section('content')
            <div class="col-md12">
                @if ( $valerror )
                <ul>
                @foreach ( $valerror as $error )
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
                @endif
                <h2>Login</h2>
                <form method="post" action="/login">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="text" id="username" name="username" />
                    <input type="password" id="password" name="password" />
                    <input type="submit" value="submit" />
                </form>
            </div>
@stop

@section('xyz')
    <h2>inheritance example</h2>
@stop



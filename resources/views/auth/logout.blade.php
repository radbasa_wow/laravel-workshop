<html>
	<head>
<!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="{{ asset('css/main.css') }}" type="text/css">
		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #B0BEC5;
				display: table;
				font-weight: 100;
				font-family: 'Lato';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 96px;
				margin-bottom: 40px;
			}

			.quote {
				font-size: 24px;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="row content">
				<div class="title highlight col-xs-12 col-sm-6 col-md-12">Laravel 5 Blade</div>
			</div>
            <div class="row">{{ $data }}</div>
            <div class="row">
				@foreach ( $products as $product )
				    {{ $product }}<br />
				@endforeach
            </div>
            <div class="row">
<div class="input-group col-xs-6 col-sm-6 col-md-8">
  <span class="input-group-addon" id="basic-addon1">@</span>
  <input type="text" class="form-control " placeholder="Username" aria-describedby="basic-addon1">
</div>
            </div>
        </div>
    </body>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</html>







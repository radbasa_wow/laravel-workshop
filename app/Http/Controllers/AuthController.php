<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

class AuthController extends Controller {

	public function __construct()
	{
        //
	}

	public function index()
	{
        return view('auth.login', ['inputval'=>[]] ); 
	}

    public function login( Request $request )
    {   
        
        // VALIDATION             
        $v = Validator::make( $request->all(),
            [
                'username' => 'required|email',
                'password' => 'required|min:5'
            ]
        );
           
        if ( $v->fails() ) {
            $messages = $v->messages();
            return view('auth.login', ['inputval' => $messages->all() ] );
        }

        $username = $request->input('username');
        $password = $request->input('password');

/*
        if ( $username == 'abc@yahoo.com' && $password == '12345' ) 
            return view('auth.profile', ['username' => $username ] );
        else
            return view('auth.login', ['inputval'=>'']);
*/

        // USING AUTH            
        if (Auth::attempt(['email' => $username, 'password' => $password]))
            return view('auth.profile', ['username' => $username ] );
        else
            return view('auth.login', ['inputval'=> ['invalid user']]);
    }
    
    public function logout()
    {
        Auth::logout();
        
        return view('auth.login', ['inputval'=>[]] ); 
    }
    
    public function profile()
    {
        return view('welcome' );
    }
}
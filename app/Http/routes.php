<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function() {
    
    if ( Auth::check() ) {
        $data = 'Passing data into views';
        //return view('welcome')->with( 'data', $data );
    
        $products = [ 'a', 'b', 'c' ];
        return view('welcome', [ 'data' => $data, 'products' => $products ] );
    }
    else {
        return view('welcome', ['data'=>'', 'products'=>[]] );
    }
   
});

/*
Route::get('login', function() {

   return view('auth.login' ); 
});

Route::post('login', function() {
    $username = Request::input('username');
    $password = Request::input('password');
    if ( $username == 'abc' && $password == '123' ) 
        return view('auth.profile', ['username' => $username ] );
    else
        return view('auth.login');
});
*/

// USING CONTROLLERS
Route::get('login', 'AuthController@index' );
Route::post('login', 'AuthController@login' );
Route::get('logout', 'AuthController@logout' );

Route::get('profile', ['middleware' => 'auth', 'uses' => 'AuthController@profile']);

Route::get('home', function() {
   $data = 'asdf';
   return "This is a new home page for " . $data; 
    
});

/*
Route::get('protected', function() {
    if ( Auth::check() ) {
        $user = Auth::user();

        echo $user->name . '<br />';
        echo $user->email . '<br />';
        return view('protected');
    } else {
        return redirect()->guest('login');
    }
} );
*/

/*
Route::get('logout', function() {
    Auth::logout();
    
    echo "LOGGED OUT";
} );
*/

Route::post('register', '' );